﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace XMLforma
{
    public partial class Form1 : Form
    {
        XmlDocument document = new XmlDocument();
        XmlElement root;
        XmlElement asmenys;
        public Form1()
        {
            InitializeComponent();
            root = document.CreateElement("root");
            document.AppendChild(root);
            asmenys = document.CreateElement("asmenys");
            root.AppendChild(asmenys);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var vardas = textBox1.Text;
            var pavarde = textBox2.Text;
            var miestas = textBox3.Text;
            var gatve = textBox4.Text;
            var namonumeris = textBox5.Text;
            XmlElement asmuo = document.CreateElement("asmuo");
            asmuo.SetAttribute("vardas", vardas);
            asmuo.SetAttribute("pavarde", pavarde);
            XmlElement adresasNode = document.CreateElement("adresas");
            XmlElement miestasNode = document.CreateElement("miestas");
            miestasNode.InnerText = miestas;
            XmlElement gatveNode = document.CreateElement("gatve");
            gatveNode.InnerText = gatve;
            XmlElement namoNumerisNode = document.CreateElement("namoNumeris");
            namoNumerisNode.InnerText = namoNumeris;
            adresasNode.AppendChild(adresasNode);
            adresasNode.AppendChild(namoNumeris);


        }
        private void isvalyklaukus()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
